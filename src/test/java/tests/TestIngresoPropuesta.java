package tests;

import configurations.CommonValues;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.IngresoPropuesta;



public class TestIngresoPropuesta extends CommonValues {

    IngresoPropuesta objPropuesta;



    @BeforeMethod
    public  void setup() {
    super.setup();
    }

    @Test
    public void ingresarPropuesta() {
        super.login("tecnico", "tecnico");
        //click menu
        objPropuesta= new IngresoPropuesta(super.getChromeDriver());
        objPropuesta.clicMenuElaboracionPropuesta();
        super.time(5);
        //getProveedor
        objPropuesta.getProveedor();
        super.time(10);
        //getCentroOperacion
        objPropuesta.getCentroOperacion();
        super.time(10);
        //getCondicionGrano
        objPropuesta.getCondicionGrano();
        super.time(20);
        //alert
        objPropuesta.alerta();
        super.time(10);
        //getProducto
        super.time(5);
        objPropuesta.getProducto();
        //setTm
        super.time(5);
        objPropuesta.settm("30");
        //setObservacion
        super.time(5);
        objPropuesta.setObservacion("Primera propuesta");
        //savePropuesta
        super.time(5);
        objPropuesta.savePropuesta();
        //alertaGuardar
        super.time(5);
        objPropuesta.alertaGuardar();

    }



    @AfterMethod
    public  void TearDown() {
        super.close();
    }


}
