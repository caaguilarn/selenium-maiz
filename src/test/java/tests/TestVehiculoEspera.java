package tests;

import configurations.CommonValues;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.IngresarVehiculoEspera;

public class TestVehiculoEspera extends CommonValues {
    IngresarVehiculoEspera objVehiculoEspera;

    @BeforeMethod
    public void setup(){
        super.setup();
    }
    @Test
    public void ingresarVehiculoEspera(){
        super.login("guardia","guardia");
        objVehiculoEspera = new IngresarVehiculoEspera(super.getChromeDriver());
        objVehiculoEspera.clicMenuReportes();
        super.time(5);
        objVehiculoEspera.adicionarRegistro();
        super.time(5);
        objVehiculoEspera.getCentroOperacion();
        super.time(5);
        objVehiculoEspera.ingresarPlaca("PCQ-2099");
        super.time(5);
        objVehiculoEspera.getProveedor(0);
        super.time(5);
        objVehiculoEspera.getEstado(2);
        super.time(5);
        objVehiculoEspera.guardarVehiculo();

    }

    @AfterMethod
    public  void TearDown() {
        super.close();
    }

}
