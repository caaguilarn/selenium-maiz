package tests;

import configurations.CommonValues;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.AprobacionPropuesta;

public class TestAprobacionPropuesta extends CommonValues {

    AprobacionPropuesta objAprobacionPropuesta;

    @BeforeMethod
    public void setup() {
        super.setup();
    }


    @Test
    public void aprobarPropuesta() {
        super.login("supervisor", "supervisor");
        //click menu
        objAprobacionPropuesta = new AprobacionPropuesta(super.getChromeDriver());
        objAprobacionPropuesta.clicMenuElaboracionPropuesta();
        //verificar
        super.time(5);
        objAprobacionPropuesta.validarRegistros();
        //enviarAprobar
        super.time(5);
        objAprobacionPropuesta.aprobar();
        //alertaEnviarAprobar
        super.time(5);
        objAprobacionPropuesta.alertaEnvio();

    }


    @AfterMethod
    public void TearDown() {
        super.close();

    }
}