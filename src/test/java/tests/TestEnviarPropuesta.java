package tests;
import configurations.CommonValues;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.EnviarAprobacion;




public class TestEnviarPropuesta extends CommonValues {

    EnviarAprobacion objEnviarAprobacion;

    @BeforeMethod
    public  void setup() {
        super.setup();
    }


    @Test
    public void enviarPropuesta() {
        super.login("tecnico", "tecnico");
        //click menu
        objEnviarAprobacion = new EnviarAprobacion(super.getChromeDriver());
        objEnviarAprobacion.clicMenuEnviarPropuesta();
        //verificar
        super.time(5);
        objEnviarAprobacion.validarRegistros();
        //enviarAprobar
        super.time(5);
        objEnviarAprobacion.enviarAprobacion();
        //alertaEnviarAprobar
        super.time(5);
        objEnviarAprobacion.alertaEnvio();

    }



    @AfterMethod
    public  void TearDown() {
        super.close();
    }
}
