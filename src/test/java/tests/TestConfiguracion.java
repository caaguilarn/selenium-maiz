package tests;

import configurations.CommonValues;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.CentroOperaciones;
import pages.PrecioFlete;


public class TestConfiguracion extends CommonValues {

    CentroOperaciones objCentroOperaciones;
    PrecioFlete objPrecio;



    @BeforeClass
    public  void setup() {
        super.setup();
    }


    @Test(priority = 1)
    public void ingresarCentroOperacion() {
        super.login("admin", "admin");
        //select Configuration menu and enter CentroOperacion screen
        super.time(8);
        objCentroOperaciones = new CentroOperaciones(super.getChromeDriver());
        super.time(5);
        objCentroOperaciones.clickAdministracion();
        // click new button
        super.time(20);
        objCentroOperaciones.clickButtonNew();
        //Screen centro de operación
        //select combobox programa
        objCentroOperaciones.getPeriodo();
        //select combobox Centro Operación
        objCentroOperaciones.getCentroOperacion();
        super.time(5);
        //setTm
        objCentroOperaciones.setTm("500");
        //SavePrecioFlete
        super.time(5);
        objCentroOperaciones.saveCentroOperacion();


    }


    @Test(priority = 2)
    public void ingresarPrecioFlete() throws InterruptedException {
        objPrecio = new PrecioFlete(super.getChromeDriver());
        Thread.sleep(10000);
        System.out.println("desaparecio el mensaje del caso anterior");
        objPrecio.clickMenuConfiguracion();
        // click new button
        super.time(2);
        objPrecio.clickButtonNew();
        // getPeriodo
        super.time(2);
        objPrecio.getPeriodo();
        //getConfiguration
        super.time(5);
        objPrecio.getConfiguracion();
        //setTm
        super.time(5);
        objPrecio.tablaPrecio();
        //savePrecioFlete
        super.time(5);
        objPrecio.savePrecioFlete();
    }







    @AfterClass
    public  void TearDown() {
        super.close();
    }



}
