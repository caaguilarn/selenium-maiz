package tests;

import configurations.CommonValues;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.ModificacionPrograma;

public class TestModificacionPrograma extends CommonValues {
    ModificacionPrograma objModificacionPrograma;

    @BeforeMethod
    public void setup(){
        super.setup();
    }

    @Test
    public void ModificacionPrograma(){
        super.login("tecnico","tecnico");
        //click menu
        objModificacionPrograma = new ModificacionPrograma(super.getChromeDriver());
        objModificacionPrograma.clicMenuProgramaAbastecimiento();
        super.time(15);
        objModificacionPrograma.validarRegistros();
        super.time(5);
        objModificacionPrograma.ingresarTm("10");
        super.time(5);
        objModificacionPrograma.ingresarMotivo("Modificación programa por cambio tm");
        super.time(5);
        objModificacionPrograma.saveModificacionPrograma();
        super.time(5);
        objModificacionPrograma.alertaGuardar();

    }

    @AfterMethod
    public  void TearDown() {
        super.close();
    }
}
