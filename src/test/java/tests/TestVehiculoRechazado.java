package tests;

import configurations.CommonValues;
import org.testng.annotations.*;
import pages.IngresarVehiculoRechazado;

public class TestVehiculoRechazado extends CommonValues {
    IngresarVehiculoRechazado objVehiculoRechazado;

    @BeforeMethod
    public void settup(){
        super.setup();
    }

    @Test
    public void ingresarVehiculoRechazado(){
        super.login("almacenera", "almacenera");
        objVehiculoRechazado = new IngresarVehiculoRechazado(super.getChromeDriver());
        super.time(10);
        objVehiculoRechazado.clicMenuReporte();
        super.time(40);
        objVehiculoRechazado.adicionarRegistro();
        super.time(10);
        objVehiculoRechazado.getCentroOperacion();
        super.time(10);
        objVehiculoRechazado.ingresarPlaca("PCD-2210");
        super.time(10);
        objVehiculoRechazado.getMotivoRechazo();
        super.time(10);
        objVehiculoRechazado.ingresarAnalista("Manuel Rivera");
        super.time(10);
        objVehiculoRechazado.getProveedor();
        super.time(10);
        objVehiculoRechazado.ingresarObservacion("observación de vehículo rechazado");
        super.time(10);
        objVehiculoRechazado.ingresarTm("10");
        super.time(50);
        objVehiculoRechazado.guardarVehiculo();
        super.time(30);
        objVehiculoRechazado.reportePdf();


    }


}
