package tests;

import configurations.CommonValues;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.AprobacionModificacionPrograma;

public class TestAprobacionModificacionPrograma extends CommonValues {
    AprobacionModificacionPrograma objAprobacionModificacionPrograma;

    @BeforeMethod
    public void setup(){
        super.setup();
    }

   @Test
    public void aprobacionModificacionPrograma(){
        super.login("supervisor","supervisor");
       //click menu
        objAprobacionModificacionPrograma = new AprobacionModificacionPrograma(super.getChromeDriver());
        objAprobacionModificacionPrograma.clicMenuProgramaAbastecimiento();
        super.time(8);
        objAprobacionModificacionPrograma.validarRegistros();
        super.time(5);
        objAprobacionModificacionPrograma.ingresarObservacion("cambio de tm aprobado");
        super.time(5);
        objAprobacionModificacionPrograma.saveAprobacionModificacionPrograma();

   }


    @AfterMethod
    public  void TearDown() {
        super.close();
    }

}
