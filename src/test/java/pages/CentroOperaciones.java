package pages;

import org.openqa.selenium.*;
import org.testng.Assert;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class CentroOperaciones {

    WebDriver driver;

    By link_menu1 = By.xpath("//a[@href='#item0']");
    By link_menu2 = By.cssSelector("div#item0 ul.nav li:nth-of-type(2)");

    By buttonNuevo =By.xpath("//div[@id='list']/div[6]/div/button/span[2]");

    By txt_periodo= By.id("activePeriodId");
    By item_periodo= By.xpath("//*[@id='activePeriodId']/div/div[5]/div[2]/ul/p-dropdownitem[2]/li");
    By buttonGuardar = By.cssSelector("#formCapRepCentro > div:nth-child(11) > button:nth-child(1) > span.ui-button-text.ui-clickable");


    public CentroOperaciones(WebDriver driver) {
        this.driver = driver;
    }


    public void clickAdministracion() {
        driver.findElement(link_menu1).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(link_menu2).click();
        driver.manage().timeouts().implicitlyWait(6, TimeUnit.SECONDS);

        if(driver.getPageSource().contains("Listado de capacidad por centro de operación")){
            System.out.println("Ingreso a la pantalla centro de operación");
        }else{
            Assert.assertTrue(false,"No ingreso a la pantalla Capacidad por centro de operación - No hizo clic en el menú");
        }

    }

    public void clickButtonNew(){
        driver.findElement(buttonNuevo).click();
        WebElement SignInButton = driver.findElement(By.id("activePeriodId"));
        Boolean checkButtonPresence = SignInButton.isDisplayed();
        Assert.assertTrue(checkButtonPresence);
        System.out.println("Ingresar Nuevo Centro de operación");
    }

    public void getPeriodo(){
        driver.findElement(txt_periodo).click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(item_periodo).click();
        System.out.println("Seleccionó periodo");

    }

    public void  getCentroOperacion() {
        driver.findElement(By.id("operationCenterId")).click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        WebElement ul = driver.findElement(By.xpath("//*[@id='operationCenterId']/div/div[5]/div[2]/ul"));
        List<WebElement> pdropown=ul.findElements(By.tagName("p-dropdownitem"));

        if (pdropown.size() != 0) {
            WebElement item = pdropown.get(0);
            item.click();
            System.out.println("Seleccionó Centro operación");

            WebElement mensaje = null;
            try {
                mensaje = driver.findElement(By.className("toast-error"));
                Assert.assertTrue(false, "Existe una configuración de centro de operación para ese periodo/centro");
            } catch (Exception e) {

            }
        }else{
            Assert.assertTrue(false,"No existe catálogo de centro de operación");
        }
    }


    public void setTm(String tm){
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        //Accedes a la tabla (tbody)
        WebElement tbody=driver.findElement(By.xpath("//*[@id=\'formCapRepCentro\']/div[4]/p-table/div/div/table/tbody"));
        //Dentro del elemento anterior vas a buscar todas las filas (Esto es una lista de tr)
        List<WebElement> listTr=tbody.findElements(By.tagName("tr"));

        //Imprimir el tamaño de la lista
        System.out.println("Cantidad de filas en la tabla tm: "+listTr.size());
        for(WebElement a:listTr){
            List<WebElement> listTd=a.findElements(By.tagName("th"));



            /**
             * Como estoy recorriendo cada fila, necesito acceder a las celdas que corresponde
             * precio(posición 2), flete(posición 3)
             */

            //Accedo a la posición 2(precio)
            //Creo un elemento que corresponde a la columna2
            WebElement columna2_tm = listTd.get(2);
            columna2_tm.click();
            columna2_tm.findElement(By.tagName("input")).clear();
            columna2_tm.findElement(By.tagName("input")).sendKeys(tm);

        }
        System.out.println("Ingreso de las tm x semana");

    }


    public void saveCentroOperacion(){
        driver.findElement(buttonGuardar).click();
        WebElement mensaje= null;
        try {
             mensaje= driver.findElement(By.className("toast-success"));
             System.out.println("Guardó configuración de centro de operación");

        }catch(Exception e) {
            Assert.assertTrue(false);

        }

    }

/*
    public void buscar(){
        WebElement mensaje= null;
        try {
            mensaje= driver.findElement(By.xpath("//*[@id=\'toast-container\']"));
        }catch(Exception e) {

        }

      if(mensaje==null){
         System.out.println("No hay mensaje");

      }else{
          System.out.println("Si hay mensaje");
      }
        driver.findElement(buttonBuscar).click();
        System.out.println("buscar");

        System.out.println("ingresar a buscar");
        WebElement tbody = driver.findElement(By.xpath("//*[@id=\"list\"]/div[6]/p-table/div/div/table/tbody"));
       List<WebElement> listTr=tbody.findElements(By.tagName("tr"));
        System.out.println("tamaño"+listTr.size());
        for (WebElement a: listTr ){
            List<WebElement> listTd=a.findElements(By.tagName("td"));
            for (WebElement b: listTd ){
                System.out.println("columnas"+ b.getText());
            }

        }


        driver.findElement(comboPeriodo).click();
        System.out.println("Seleccionar periodo a buscar");
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(itemcomboPeriodo).click();
        System.out.println("escogo periodo");
        driver.findElement(buttonBuscar).click();
        System.out.println("buscar");
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

    }
*/
}
