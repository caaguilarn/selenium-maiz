package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class IngresarVehiculoRechazado {
    WebDriver driver;

    By link_menu= By.xpath("//a[@href='#item0']");
    By link_menu1= By.xpath("//*[@id=\'item0\']/ul/li[3]/a");
    By buttonNuevo= By.xpath("//button[@label='Nuevo']");
    By comboCentro= By.xpath("//p-dropdown[@id='operationCenterId']/div/div[5]/div[2]/ul");
    By txt_placa = By.xpath("//*[@id=\'plateIdFormId\']/input");
    By comboMotivo= By.xpath("//p-dropdown[@id='reasonsRejectedId']/div/div[5]/div[2]/ul");
    By txt_analista= By.xpath("//*[@id=\'analisId\']");
    By comboProveedor= By.xpath("//p-dropdown[@id='providersFormId']/div/div[5]/div[2]/ul");
    By txt_observacion= By.xpath("//*[@id='remarkId']");
    By txt_tm= By.xpath("//*[@id='tmRejectedId']");
    By buttonGuardar= By.xpath("//*[@id=\"form\"]/p-table/div/div/table/tbody/tr/td/div/p-button[1]");
    By buttonReporte= By.xpath("//img[@src='/assets/img/icon_pdf.png']");

    public IngresarVehiculoRechazado(WebDriver driver) {
        this.driver = driver;
    }

    public void clicMenuReporte(){
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.findElement(link_menu).click();
        driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
        driver.findElement(link_menu1).click();
        if(driver.getPageSource().contains("Vehículos Rechazados")){
            System.out.println("Ingreso de vehículos rechazados");
        }else{
            Assert.assertTrue(false,"No ingreso pantalla vehículo rechazado");
        }
        driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
    }

    public void adicionarRegistro(){
        driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
        driver.findElement(buttonNuevo).click();
        driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
        System.out.println("Adicionar nuevo registro de vehículo rechazado");
    }

    public void getCentroOperacion() {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        JavascriptExecutor js = (JavascriptExecutor) driver ;
        js.executeScript("scroll(0, 250);");

        WebElement txtarea1 = driver.findElement(By.xpath("//*[@id=\'operationCenterId\']/div/div[4]"));
        Actions toolAct= new Actions(driver);
        toolAct.moveToElement(txtarea1).perform();
        toolAct.moveToElement(txtarea1).click().perform();

        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        WebElement ul = driver.findElement(comboCentro);
        List<WebElement> pdropown=ul.findElements(By.tagName("p-dropdownitem"));
        if (pdropown.size() != 0) {
            WebElement item = pdropown.get(0);
            item.click();
            System.out.println("Seleccionó Centro operación");
        }else{
            Assert.assertTrue(false,"No existe catálogo de centro de operación");
        }
    }
    public void ingresarPlaca(String placa) {
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.findElement(txt_placa).click();
        driver.findElement(txt_placa).sendKeys(placa);
        System.out.println("Ingresó placa");
    }


    public void getMotivoRechazo() {
        driver.findElement(By.id("reasonsRejectedId")).click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        WebElement ul = driver.findElement(comboMotivo);
        List<WebElement> pdropown=ul.findElements(By.tagName("p-dropdownitem"));
        if (pdropown.size() != 0) {
            WebElement item = pdropown.get(1);
            item.click();
            System.out.println("Seleccionó Motivo Rechazo");
        }else{
            Assert.assertTrue(false,"No existe catálogo de motivo de rechazo");
        }
    }


    public void ingresarAnalista(String analista) {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(txt_analista).click();
        driver.findElement(txt_analista).sendKeys(analista);
        System.out.println("Ingresó analista");
    }


    public void getProveedor() {
        driver.findElement(By.id("providersFormId")).click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        WebElement ul = driver.findElement(comboProveedor);
        List<WebElement> pdropown=ul.findElements(By.tagName("p-dropdownitem"));
        if (pdropown.size() != 0) {
            WebElement item = pdropown.get(1);
            item.click();
            System.out.println("Seleccionó Proveedor");
        }else{
            Assert.assertTrue(false,"No existe proveedor");
        }
    }

    public void ingresarObservacion(String observacion) {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(txt_observacion).click();
        driver.findElement(txt_observacion).sendKeys(observacion);
        System.out.println("Ingresó observación");
    }

    public void ingresarTm(String tm) {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(txt_tm).click();
        driver.findElement(txt_tm).sendKeys(tm);
        System.out.println("Ingresó tm");
    }

   public void guardarVehiculo(){
        driver.findElement(buttonGuardar).click();
        WebElement mensaje;
       try {
           mensaje = driver.findElement(By.className("toast-success"));
           System.out.println("Guardar vehículo rechazado");

       } catch (Exception e) {

       }
   }
   public void reportePdf(){
       driver.findElement(buttonReporte).click();
       driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
       System.out.println("Abrir reporte pdf");
       driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
       driver.quit();
   }

}
