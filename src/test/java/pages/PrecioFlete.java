package pages;


import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class PrecioFlete {

    WebDriver driver;

    By link_menu1 = By.xpath("//a[@href='#item0']");
    By link_menu2= By.cssSelector("div#item0 ul.nav li:nth-of-type(3)");
    By buttonNuevo =By.xpath("//*[@id='list']/div[6]/div[1]/button/span[2]");
    By txt_periodo= By.id("activePeriodId");
    By item_periodo= By.xpath("//*[@id='activePeriodId']/div/div[5]/div[2]/ul/p-dropdownitem[2]/li");
    By txt_configuracion= By.id("configurationsId");
    By buttonGuardar= By.cssSelector("#formPreFle > div:nth-child(11) > button:nth-child(1)");


    public PrecioFlete(WebDriver driver){
        this.driver=driver;
    }

    public void clickMenuConfiguracion() {
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        WebElement menu= driver.findElement(link_menu1);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        WebElement lista=driver.findElement(By.xpath("//*[@id=\'item0\']/ul"));
        List<WebElement> li=lista.findElements(By.tagName("li"));
        WebElement menuPrecioFlete = li.get(2);
        menuPrecioFlete.click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        if(driver.getPageSource().contains("Listado de condiciones de precio y flete")){
            System.out.println("Ingreso a la pantalla precio y flete");
        }else{
           Assert.assertTrue(false,"No ingreso a la pantalla precio y flete");
        }

    }

    public void clickButtonNew(){
        driver.findElement(buttonNuevo).click();
        WebElement SignInButton = driver.findElement(By.id("activePeriodId"));
        Boolean checkButtonPresence = SignInButton.isDisplayed();
        Assert.assertTrue(checkButtonPresence);
        System.out.println("Ingresar Nuevo Precio y flete");
    }


    public void getPeriodo(){
        driver.findElement(txt_periodo).click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(item_periodo).click();
        System.out.println("Seleccionó periodo");
    }

   public void getConfiguracion(){
       driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
        driver.findElement(txt_configuracion).click();
       WebElement ul = driver.findElement(By.xpath("//*[@id='configurationsId']/div/div[5]/div[2]/ul"));
       List<WebElement> pdropown=ul.findElements(By.tagName("p-dropdownitem"));
       if(pdropown.size()!=0) {
           WebElement item = pdropown.get(1);
           item.click();
       }else{
           Assert.assertTrue(false,"No existe configuración");
       }
    /*    driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
        JavascriptExecutor js = (JavascriptExecutor) driver ;
        WebElement opciones = driver.findElement(By.xpath("//*[@id='configurationsId']/div/div[5]/div[2]/ul/p-dropdownitem[2]/li"));
        js.executeScript("arguments[0].click();", opciones);
        opciones.click();
      */
        WebElement mensaje= null;
        try {
           mensaje= driver.findElement(By.className("toast-error"));
            Assert.assertTrue(false,"Existe configuración de precio y flete para el periodo");
        }catch(Exception e) {

        }
        System.out.println("Seleccionó configuración");
   }

   public void tablaPrecio(){
       Random rand = new Random();
       //Accedes a la tabla (tbody)
       WebElement tbody = driver.findElement(By.xpath("//*[@id='formPreFle']/div[4]/p-table/div/div/table/tbody"));

       //Dentro del elemento anterior vas a buscar todas las filas (Esto es una lista de tr)
       List<WebElement> listTr=tbody.findElements(By.tagName("tr"));

       //Imprimir el tamaño de la lista
       System.out.println("Cantidad de filas en la tabla precio y flete: "+listTr.size());

       //Obtener la lista de columnas de cada una de las filas
       //Recorrer cada fila
       for (WebElement a: listTr ){

           //Obtener todas las columnas (Esto es una lista de td)
           List<WebElement> listTd=a.findElements(By.tagName("td"));

           /**
            * Como estoy recorriendo cada fila, necesito acceder a las celdas que corresponde
            * precio(posición 2), flete(posición 3)
            */

           //Accedo a la posición 2(precio)
           //Creo un elemento que corresponde a la columna2
           WebElement columna2_precio = listTd.get(2);
           columna2_precio.click();
           columna2_precio.findElement(By.tagName("input")).clear();
           //rand.nextInt(1000) -> Esta línea devuelve un ENTERO (RANDOMICO) en el rango de número que esta en el parentesis
           columna2_precio.findElement(By.tagName("input")).sendKeys(""+rand.nextInt(100)+1);

           //Accedo a la posición 3(flete)
           //Creo un elemento que corresponde a la columna3
           WebElement columna3_flete = listTd.get(3);
           columna3_flete.click();
           columna3_flete.findElement(By.tagName("input")).clear();
           columna3_flete.findElement(By.tagName("input")).sendKeys(""+rand.nextInt(100)+1);


       }


   }


   public void savePrecioFlete(){
      driver.findElement(buttonGuardar).click();
       WebElement mensaje= null;
       try {
           mensaje= driver.findElement(By.className("toast-success"));
           System.out.println("Guardó registro de configuración de precio y flete");

       }catch(Exception e) {
           Assert.assertTrue(false);

       }



   }


}
