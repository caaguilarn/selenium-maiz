package pages;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class AprobacionPropuesta {
    WebDriver driver;
    By link_menu1 = By.xpath("//a[@href='#item0']");
    By link_menu2 = By.xpath("//*[@id=\"item0\"]/ul/li[1]");

    public AprobacionPropuesta(WebDriver driver) {
        this.driver = driver;

    }


    public void clicMenuElaboracionPropuesta() {
        driver.manage().timeouts().implicitlyWait(6, TimeUnit.SECONDS);
        driver.findElement(link_menu1).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(link_menu2).click();
        if (driver.getPageSource().contains("Aprobación de Propuesta")) {
            System.out.println("Ingreso a la pantalla Aprobación de propuesta");
        } else {
            Assert.assertTrue(false,"No ingreso a la pantalla Aprobación propuesta");
        }

    }


    public void validarRegistros() {
        WebElement tbody = driver.findElement(By.xpath("//p-table/div/div/div[2]/div[2]/table/tbody"));
        List<WebElement> listTr = tbody.findElements(By.tagName("tr"));
        //Imprimir el tamaño de la lista
        System.out.println("Cantidad de registros: " + listTr.size());

        boolean estadoPendienteAprobado = false;
        for (WebElement a : listTr) {
            //Obtener todas las columnas (Esto es una lista de td)
            List<WebElement> listTd = a.findElements(By.tagName("td"));

            if (listTd.get(24).getText().equals("Pendiente de Aprobación")) {
                estadoPendienteAprobado = true;
                break;
            }
        }
        if (estadoPendienteAprobado) {
            System.out.println("Hay propuestas en estado pendiente de aprobación");
            Assert.assertTrue(true);
        } else {
            System.out.println("Hay propuestas en estado diferentes a pendiente de aprobación");
            Assert.assertTrue(false, "No hay propuestas en estado pendiente de aprobación");
        }


    }

    public void aprobar() {
        WebElement button = driver.findElement(By.xpath("//button[@label='Aprobar']"));
        Actions toolbtn = new Actions(driver);
        toolbtn.moveToElement(button).click().perform();
        System.out.println("Se presiono el botón aprobar");

    }

    public void alertaEnvio() {
            driver.findElement(By.xpath("//p-confirmdialog/div/div[3]")).click();
            System.out.println("clic en cuadro dialogo");
            driver.findElement(By.xpath("//p-confirmdialog/div/div[3]/p-footer/button[2]")).click();
            System.out.println("Confirmar la aprobación de las propuestas");
        WebElement mensaje = null;
        try {
            mensaje = driver.findElement(By.className("toast-success"));
            System.out.println("Se creo el programa");

        } catch (Exception e) {

        }

    }



}