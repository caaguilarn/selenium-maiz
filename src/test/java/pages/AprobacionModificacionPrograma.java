package pages;


import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class AprobacionModificacionPrograma {
    WebDriver driver;
    By link_menu1 = By.xpath("//a[@href='#item1']");
    By link_menu2 = By.xpath("//*[@id=\"item1\"]/ul/li[1]");

    public AprobacionModificacionPrograma(WebDriver driver) {
        this.driver = driver;
    }

    public void clicMenuProgramaAbastecimiento(){
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        driver.findElement(link_menu1).click();
        driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
        driver.findElement(link_menu2).click();
        if(driver.getPageSource().contains("Aprobación de Modificación de Programa")){
            System.out.println("Ingreso a la pantalla Aprobación de Modificación de Programa");
        }else{
            Assert.assertTrue(false,"No ingreso pantalla Aprobación de Modificación de Programa");

        }
    }

    public void validarRegistros() {
        driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
        WebElement tbody = driver.findElement(By.xpath("//p-table/div/div/table/tbody"));
        List<WebElement> listTr = tbody.findElements(By.tagName("tr"));
        if (listTr.size()!=0) {
            //Obtener todas las columnas (Esto es una lista de td)
            for (WebElement a : listTr) {
                List<WebElement> listTd = a.findElements(By.tagName("td"));
                if (listTd.size() != 0) {
                    a.click();
                    System.out.println("Ingresó pantalla datos modificados");


                } else {
                    Assert.assertTrue(false, "No hay registros para ser aprobados");
                }

            }
        }else{
            Assert.assertTrue(false, "No hay registros para ser aprobados");
        }

    }

    public void ingresarObservacion(String observacion) {
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        //ir a la parte de abajo de la pantalla
        js.executeScript("scroll(0, 250);");
        if(driver.getPageSource().contains("Observación")){
            WebElement txtarea1 = driver.findElement(By.xpath("//textarea[@id='remarkId']"));
            Actions toolAct = new Actions(driver);
            toolAct.moveToElement(txtarea1).perform();
            toolAct.moveToElement(txtarea1).click().perform();
            txtarea1.sendKeys(observacion);
        }else{
            Assert.assertTrue(false,"No ingreso a la pantalla de datos modificados");
        }

    }


    public void saveAprobacionModificacionPrograma(){
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        WebElement button =driver.findElement(By.xpath("//form/div[2]/div/button[1]/span[2]"));
        Actions toolCheck=new Actions(driver);
        toolCheck.moveToElement(button).perform();
        toolCheck.moveToElement(button).click().perform();
        WebElement mensaje= null;
        try {
            mensaje= driver.findElement(By.className("toast-success"));
            System.out.println("guardó la aprobación del programa");

        }catch(Exception e) {
        }

    }




}
