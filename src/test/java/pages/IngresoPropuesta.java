package pages;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class IngresoPropuesta {

    WebDriver driver;
    By link_menu1 = By.xpath("//a[@href='#item0']");
    By link_menu2 = By.xpath("//*[@id=\"item0\"]/ul/li[1]");


    public IngresoPropuesta(WebDriver driver){
    this.driver= driver;
    }


    public void clicMenuElaboracionPropuesta(){
        driver.manage().timeouts().implicitlyWait(6,TimeUnit.SECONDS);
        driver.findElement(link_menu1).click();
        driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
        driver.findElement(link_menu2).click();
        if(driver.getPageSource().contains("Ingreso de Nueva Propuesta")){
            System.out.println("Ingreso a la pantalla Propuesta");
        }else{
            Assert.assertTrue(false,"No ingreso a la pantalla de propuesta");
        }
    }

    public void getProveedor(){
        driver.findElement(By.id("providersFormId")).click();
        WebElement ul = driver.findElement(By.xpath("//*[@id='providersFormId']/div/div[5]/div[2]/ul"));
        List<WebElement> pdropown=ul.findElements(By.tagName("p-dropdownitem"));
        if(pdropown.size()!=0) {
            WebElement item = pdropown.get(0);
            item.click();
            System.out.println("Seleccionó Proveedor");
        }else {
            Assert.assertTrue(false, "No existe proveedores asociados");
        }
    }


    public void getCentroOperacion(){
        driver.findElement(By.id("operationCenterId")).click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        WebElement ul = driver.findElement(By.xpath("//*[@id='operationCenterId']/div/div[5]/div[2]/ul"));
        List<WebElement> pdropown=ul.findElements(By.tagName("p-dropdownitem"));
        if(pdropown.size()!=0) {

            WebElement item = pdropown.get(0);
            item.click();
            System.out.println("Seleccionó Centro operación");
        }else{
            Assert.assertTrue(false,"No existe catálogo de centro de operación");
        }
    }

    public void getCondicionGrano(){
        driver.findElement(By.id("grainConditionsId")).click();
        WebElement ul = driver.findElement(By.xpath("//*[@id='grainConditionsId']/div/div[5]/div[2]/ul"));
        List<WebElement> pdropown=ul.findElements(By.tagName("p-dropdownitem"));
        WebElement item=pdropown.get(0);
        item.click();
        System.out.println("Seleccionó Condicion grano");

    }


    public void alerta(){
       driver.findElement(By.xpath("//p-confirmdialog/div/div[3]")).click();
       driver.findElement(By.xpath("//p-confirmdialog/div/div[3]/p-footer/button[2]")).click();
       System.out.println("Aceptar alerta de actualización de datos");
    }

    public void getProducto() {
        driver.findElement(By.id("productsId")).click();
        WebElement ul = driver.findElement(By.xpath("//*[@id='productsId']/div/div[5]/div[2]/ul"));
        List<WebElement> pdropown = ul.findElements(By.tagName("p-dropdownitem"));
        WebElement item = pdropown.get(0);
        item.click();
        System.out.println("Seleccionó Producto");
    }

    public void settm(String tm) {
        Random rand = new Random();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        //Accedes a la tabla (tbody)
        WebElement tbody = driver.findElement(By.xpath("//*[@id='list']/form/p-table/div/div/table/tbody"));
        //Dentro del elemento anterior vas a buscar todas las filas (Esto es una lista de tr)
        List<WebElement> listTr = tbody.findElements(By.tagName("tr"));
        //Imprimir el tamaño de la lista
        System.out.println("Cantidad de filas en la tabla tm a programar: " + listTr.size());
        if(listTr.size()==0){
            Assert.assertTrue(false,"No se cargó la configuración de centro, precio y flete, existe propuesta o  programa");
        }else {
            for (WebElement a : listTr) {
                List<WebElement> listTd = a.findElements(By.tagName("td"));
                WebElement capacidad=listTd.get(5);
                if(capacidad.getText().equals("0")){
                    Assert.assertTrue(false,"La capacidad disponible es 0");
                }else {
                    /**
                     * Como estoy recorriendo cada fila, necesito acceder a las celdas que corresponde
                     * tmprogramar
                     */

                    //Accedo a la posición 1
                    //Creo un elemento que corresponde a la columna2
                    WebElement columna1_tm = listTd.get(1);
                    columna1_tm.click();
                    columna1_tm.findElement(By.tagName("input")).clear();
                    //rand.nextInt(1000) -> Esta línea devuelve un ENTERO (RANDOMICO) en el rango de número que esta en el parentesis
                    columna1_tm.findElement(By.tagName("input")).sendKeys(tm);
                }

            }
        }

    }

    public void setObservacion (String observacion){
        WebElement txtarea1 = driver.findElement(By.xpath("//textarea[@id='remarkId']"));
        Actions toolAct= new Actions(driver);
        toolAct.moveToElement(txtarea1).click().perform();
        txtarea1.sendKeys(observacion);
        System.out.println("Ingresó observación");

    }

    public void savePropuesta(){
        WebElement button= driver.findElement(By.xpath("//*[@id='list']/form/div[5]/div/button[1]"));
        Actions toolbtn= new Actions(driver);
        toolbtn.moveToElement(button).click().perform();
        WebElement mensaje= null;
        try {
            mensaje= driver.findElement(By.className("toast-success"));
            System.out.println("Guardó propuesta");
        }catch(Exception e) {

        }
    }

    public void alertaGuardar(){
        if(driver.getPageSource().contains("Registro guardado")){
            driver.findElement(By.xpath("//p-confirmdialog/div/div[3]")).click();
            System.out.println("clic en cuadro dialogo");
            driver.findElement(By.xpath("//p-confirmdialog/div/div[3]/p-footer/button[1]")).click();
            System.out.println("No aceptar posibilidad de ingreso de nueva propuesta");
            if(driver.getPageSource().contains("Inicio")){
                System.out.println("Pagina de inicio");
            }else{
                System.out.println("No se presionó la opción de No en la alerta");
            }
        }else{
            System.out.println("No se mostró cuadro de alerta");
        }


    }

}
