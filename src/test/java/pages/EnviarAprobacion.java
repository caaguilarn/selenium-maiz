package pages;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class EnviarAprobacion {
    WebDriver driver;
    By link_menu1 = By.xpath("//a[@href='#item0']");
    By link_menu2 = By.xpath("//*[@id=\'item0\']/ul/li[2]");


    public EnviarAprobacion(WebDriver driver) {
        this.driver = driver;
    }


    public void clicMenuEnviarPropuesta() {
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.findElement(link_menu1).click();
        driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
        driver.findElement(link_menu2).click();
        if (driver.getPageSource().contains("Envío / Modificación Propuesta")) {
            System.out.println("Ingreso a la pantalla Envío / Modificación ");
        } else {
            Assert.assertTrue(false,"No ingreso a la pantalla Envío / Modificación");
        }

    }

    public void validarRegistros() {
        WebElement tbody = driver.findElement(By.xpath("//p-table/div/div/div[2]/div[2]/table/tbody"));
        List<WebElement> listTr = tbody.findElements(By.tagName("tr"));
        //Imprimir el tamaño de la lista
        System.out.println("Cantidad de filas en la tabla: " + listTr.size());
        boolean estadoPendiente = false;
        for (WebElement a : listTr) {
            //Obtener todas las columnas (Esto es una lista de td)
            List<WebElement> listTd = a.findElements(By.tagName("td"));

            if (listTd.get(24).getText().equals("Pendiente")) {
                estadoPendiente = true;
                break;
            }
        }
        if (estadoPendiente) {
            System.out.println("Existen propuestas en estado pendiente");
            Assert.assertTrue(true);
        } else {
            System.out.println("Existen propuestas en estado diferentes a pendiente");
            Assert.assertTrue(false,"No hay propuestas en estado pendiente que se puedan enviar aprobar");
        }


    }

    public void enviarAprobacion() {
        WebElement button = driver.findElement(By.xpath("//button[@label='Enviar para aprobación']"));
        Actions toolbtn = new Actions(driver);
        toolbtn.moveToElement(button).click().perform();
        System.out.println("Presionar el botón Enviar aprobación");

    }

    public void alertaEnvio() {
        if (driver.getPageSource().contains("Enviar propuestas aprobación")) {
            driver.findElement(By.xpath("//p-confirmdialog/div/div[3]")).click();
            driver.findElement(By.xpath("//p-confirmdialog/div/div[3]/p-footer/button[2]")).click();
            System.out.println("Confirmar el envío de las propuestas en estado pendiente aprobación");
            WebElement mensaje = null;
            try {
                mensaje = driver.findElement(By.className("toast-success"));

            } catch (Exception e) {

            }


        }else{
            System.out.println("no entra en el cuadro de confirmación");
        }

    }
}
