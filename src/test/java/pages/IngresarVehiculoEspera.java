package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class IngresarVehiculoEspera {
    WebDriver driver;

    By link_menu = By.xpath("//a[@href='#item0']");
    By link_menu1 = By.xpath("//*[@id=\'item0\']/ul/li/a");
    By buttonNuevo = By.xpath("//button[@label='Nuevo']");
    By comboCentro= By.xpath("//p-dropdown[@id='operationCenterId']/div/div[5]/div[2]/ul");
    By txt_placa = By.xpath("//*[@id=\'plateIdFormId\']/input");
    By buttonGuardar= By.xpath("//*[@id='formWv']/p-table/div/div/table/tbody/tr/td/div/p-button[1]");


    public IngresarVehiculoEspera(WebDriver driver) {
        this.driver = driver;
    }

    public void clicMenuReportes() {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(link_menu).click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.findElement(link_menu1).click();
        if (driver.getPageSource().contains("Vehículos en Espera")) {
            System.out.println("Ingreso pantalla vehículo en espera");
        } else {
            Assert.assertTrue(false, "No ingreso pantalla vehículo en espera");

        }
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    public void adicionarRegistro() {
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(buttonNuevo).click();
        System.out.println("Adicionar nuevo registro de vehículo en espera");
    }

    public void getCentroOperacion() {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        JavascriptExecutor js = (JavascriptExecutor) driver ;
        js.executeScript("scroll(0, 250);");

        WebElement txtarea1 = driver.findElement(By.xpath("//*[@id=\'operationCenterId\']/div/div[4]"));
        Actions toolAct= new Actions(driver);
        toolAct.moveToElement(txtarea1).perform();
        toolAct.moveToElement(txtarea1).click().perform();


        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        WebElement ul = driver.findElement(comboCentro);
        List<WebElement> pdropown=ul.findElements(By.tagName("p-dropdownitem"));
        if (pdropown.size() != 0) {
            WebElement item = pdropown.get(0);
            item.click();
            System.out.println("Seleccionó Centro operación");
        }else{
            Assert.assertTrue(false,"No existe catálogo de centro de operación");
        }
    }

    public void ingresarPlaca(String placa) {

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(txt_placa).click();
        driver.findElement(txt_placa).sendKeys(placa);
        System.out.println("Ingresar placa");
    }

    public void getProveedor(int posicion) {
        driver.findElement(By.id("providersFormId")).click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        WebElement ul = driver.findElement(By.xpath("//p-dropdown[@id='providersFormId']/div/div[5]/div[2]/ul"));
        List<WebElement> pdropown=ul.findElements(By.tagName("p-dropdownitem"));
        if (pdropown.size() != 0) {
            WebElement item = pdropown.get(posicion);
            item.click();
           System.out.println("Seleccionó Proveedor");
        }else{
            Assert.assertTrue(false,"No existe proveedores");
        }
    }


    public void  getEstado(int posicion) {
        driver.findElement(By.id("waitingStateId")).click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        WebElement comboEstado = driver.findElement(By.xpath("//p-dropdown[@id='waitingStateId']/div/div[5]/div[2]/ul"));
        List<WebElement> pdropown= comboEstado.findElements(By.tagName("p-dropdownitem"));
        if(pdropown.size()!=0){
            WebElement item=pdropown.get(posicion);
            item.click();
            System.out.println("Seleccionó Estado");
        }else{
            Assert.assertTrue(false,"No existe catálogo de estado de vehículo en espera");
        }


    }

    public  void guardarVehiculo(){
        driver.findElement(buttonGuardar).click();
        WebElement mensaje;
        try {
            mensaje = driver.findElement(By.className("toast-success"));
            System.out.println("Guardar vehículo en espera");

        } catch (Exception e) {

        }

    }

}
