package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class ModificacionPrograma {

    WebDriver driver;
    By link_menu1 = By.xpath("//a[@href='#item1']");
    By link_menu2 = By.xpath("//*[@id=\"item1\"]/ul/li[1]");

    public ModificacionPrograma(WebDriver driver){
        this.driver= driver;

    }


    public void clicMenuProgramaAbastecimiento(){
        driver.manage().timeouts().implicitlyWait(6, TimeUnit.SECONDS);
        driver.findElement(link_menu1).click();
        driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
        driver.findElement(link_menu2).click();
        if(driver.getPageSource().contains("Modificación de Programa")){
            System.out.println("Ingreso a la pantalla Modificación de Programa");
        }else{
            Assert.assertTrue(false,"No ingreso a la pantalla Modificación de Programa");
        }

    }


    public void validarRegistros() {
        WebElement tbody = driver.findElement(By.xpath("//p-table/div/div/table/tbody"));
        List<WebElement> listTr = tbody.findElements(By.tagName("tr"));
        if (listTr.size()!=0) {
            for (WebElement a : listTr) {
                //Obtener todas las columnas (Esto es una lista de td)
                List<WebElement> listTd = a.findElements(By.tagName("td"));
                if (listTd.get(4).getText().equals("Aprobado")) {
                    a.click();
                    break;

                } else {
                    System.out.println("el registro no tiene estado aprobado");
                }

            }
        }else{
            Assert.assertTrue(false, "No hay registros para ser modificados");
        }

        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

    }

    public void ingresarTm(String tm){
        WebElement tbody = driver.findElement(By.xpath("//*[@id='list']/form/p-table/div/div/table/tbody"));
        //Dentro del elemento anterior vas a buscar todas las filas (Esto es una lista de tr)
        List<WebElement> listTr = tbody.findElements(By.tagName("tr"));

           for (WebElement a : listTr) {
            List<WebElement> listTd = a.findElements(By.tagName("td"));
            /**
             * Como estoy recorriendo cada fila, necesito acceder a las celdas que corresponde
             * tmprogramar
             */

            //Accedo a la posición 1
            //Creo un elemento que corresponde a la columna2
            WebElement columna1_tm = listTd.get(1);

            columna1_tm.click();
            //rand.nextInt(1000) -> Esta línea devuelve un ENTERO (RANDOMICO) en el rango de número que esta en el parentesis

               columna1_tm.findElement(By.tagName("input")).clear();
            columna1_tm.findElement(By.tagName("input")).sendKeys(tm);

        }
        System.out.println("Modificó datos de tm");
    }


    public void ingresarMotivo(String observacion){
        driver.manage().timeouts().implicitlyWait(8, TimeUnit.SECONDS);
        JavascriptExecutor js = (JavascriptExecutor) driver ;
        js.executeScript("scroll(0, 250);");

        WebElement txtarea1 = driver.findElement(By.xpath("//textarea[@id='remarkId']"));
        Actions toolAct= new Actions(driver);
        toolAct.moveToElement(txtarea1).perform();
        toolAct.moveToElement(txtarea1).click().perform();
        txtarea1.sendKeys(observacion);


        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        WebElement check =driver.findElement(By.xpath("//p-checkbox[@id='group' and @ng-reflect-label='Cambio de tm a programar']"));
        Actions toolCheck=new Actions(driver);
        toolCheck.moveToElement(check).perform();
        toolCheck.moveToElement(check).click().perform();
    }
    public void saveModificacionPrograma(){
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        WebElement button =driver.findElement(By.xpath("//*[@id=\"list\"]/form/div[6]/div/button[1]"));
        Actions toolCheck=new Actions(driver);
        toolCheck.moveToElement(button).perform();
        toolCheck.moveToElement(button).click().perform();

        WebElement mensaje= null;
        try {
            mensaje= driver.findElement(By.className("toast-success"));
            System.out.println("Guardó modificación de programa");
        }catch(Exception e) {
            Assert.assertTrue(false,"No se puede guardar modificación");
        }
    }

    public void alertaGuardar(){
        if(driver.getPageSource().contains("Registro guardado")){
            driver.findElement(By.xpath("//p-confirmdialog/div/div[3]")).click();
            driver.findElement(By.xpath("//p-confirmdialog/div/div[3]/p-footer/button[1]")).click();
            System.out.println("No acepta crear un nuevo programa");
            if(driver.getPageSource().contains("Modificación de Programa")){
                System.out.println("Pantalla principal");
            }
        }else{
            System.out.println("No ingreso a la alerta");
        }


    }



}
