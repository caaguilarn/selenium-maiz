package configurations;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


import java.util.concurrent.TimeUnit;

public class CommonValues {

    private String url = "https://cdabates.pronaca.com/home";
    private WebDriver chromeDriver;
    By txt_user = By.name("username");
    By txt_password = By.name("password");
    By btn_login = By.name("login");

    /**
     * Constructor inicializa objetos
     * driver
     **/
    public CommonValues() {
       System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\driver\\chromedriver.exe");
        chromeDriver  = new ChromeDriver();
    }

    public void setup() {
         chromeDriver.get(url);
         chromeDriver.manage().window().maximize();
         chromeDriver.manage().timeouts().implicitlyWait(80,TimeUnit.SECONDS);
         if(chromeDriver.getTitle().equals("Inicia sesión en pronaca")){
             System.out.println("Ingreso a la aplicación de maíz");
         }else{
             org.testng.Assert.assertTrue(false,"No accedió a la url");
         }

         chromeDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    /**
     * Este metodo tengo que enviar el usuario y la contraseña pantalla login
     * @param user
     * @param pass
     */
    public void login(String user, String pass){
        chromeDriver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
        chromeDriver.findElement(txt_user).sendKeys(user);
        chromeDriver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        chromeDriver.findElement(txt_password).sendKeys(pass);
        chromeDriver.findElement(btn_login).click();
        chromeDriver.manage().timeouts().implicitlyWait(7, TimeUnit.SECONDS);
        Assert.assertEquals("Compra de maíz", chromeDriver.getTitle());
        chromeDriver.manage().timeouts().implicitlyWait(8, TimeUnit.SECONDS);
    }

    /**
     * Metodo que permite dar un tiempo en la ejecución de la aplicación se debe enviar seconds
     * @param seconds
     */
    public void time(int seconds){

        chromeDriver.manage().timeouts().implicitlyWait(seconds, TimeUnit.SECONDS);
    }



    public void close(){
        chromeDriver.manage().deleteAllCookies();
        chromeDriver.close();
    }


    /**
     * Metodo para retornar la variable
     * @return
     */
    public WebDriver getChromeDriver() {
        return chromeDriver;
    }

    public void setChromeDriver(WebDriver chromeDriver) {
        this.chromeDriver = chromeDriver;
    }
}
