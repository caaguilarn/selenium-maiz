/*
 * 5 setencias sql que permite agregar registro en la tabla catalogo
 * Centro de operacion que corresponde al catalogo padre(4)
 * Producto que corresponde al catalogo padre(28)
 * Condicion grano que corresponde al catalogo padre(31)
 * Motivo rechazo de vehiculos que corresponde al catalogo padre(43)
 * Estado de vehiculo en espera que corresponde al catalogo padre(46)
 */



INSERT INTO public.catalogo (id_catalogo_padre,nombre_catalogo,descripcion1,descripcion2,observacion,estado,fecha_registro,usuario_registro,fecha_actualizacion,usuario_actualizacion)
VALUES (4,'CENTRO PRUEBA','CENTRO PRUEBA','DESCRIPCION2','OBSERVACION',1,current_date,'admin',NULL,NULL) returning id_catalogo;


INSERT INTO public.catalogo (id_catalogo_padre,nombre_catalogo,descripcion1,descripcion2,observacion,estado,fecha_registro,usuario_registro,fecha_actualizacion,usuario_actualizacion)
VALUES (28,'Ma�z Amarillo Nacional','Ma�z Amarillo Nacional','DESCRIPCION2','OBSERVACION',1,current_date,'admin',NULL,NULL) returning id_catalogo;



INSERT INTO public.catalogo (id_catalogo_padre,nombre_catalogo,descripcion1,descripcion2,observacion,estado,fecha_registro,usuario_registro,fecha_actualizacion,usuario_actualizacion)
VALUES (31,'Seco','Seco','DESCRIPCION2','OBSERVACION',1,current_date,'admin',NULL,NULL)  returning id_catalogo;


INSERT INTO public.catalogo (id_catalogo_padre,nombre_catalogo,descripcion1,descripcion2,observacion,estado,fecha_registro,usuario_registro,fecha_actualizacion,usuario_actualizacion)
VALUES (43,'Granos con insectos','Granos con insectos','DESCRIPCION2','OBSERVACION',1,current_date,'admin',NULL,NULL) returning id_catalogo;


INSERT INTO public.catalogo (id_catalogo_padre,nombre_catalogo,descripcion1,descripcion2,observacion,estado,fecha_registro,usuario_registro,fecha_actualizacion,usuario_actualizacion)
VALUES (46,'Proceso de An�lisis','Proceso de An�lisis','DESCRIPCION2','OBSERVACION',1,current_date,'admin',NULL,NULL) returning id_catalogo;





