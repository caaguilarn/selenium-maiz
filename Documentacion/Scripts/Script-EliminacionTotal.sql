/*
 * 11 setencias sql que permiten eliminar todos
 * los registros de las tablas
 * tabla motivo_cambio, observaciones,propuesta, propuesta, programa
 * condicion precio y flete, condicion precio y flete_detalle. 
 * centro operacion 1 registro, centro operacion_detalles.
 * vehiculo en espera 1 registro,vehiculo rechazado 1 registro.
 */

delete from propuesta_motivo_cambio;
delete from observaciones;
delete from propuesta_det;
delete from propuesta;
delete from programa;
delete from  condicion_precio_flete_det;
delete from condicion_precio_flete_cab;
delete from capacidad_centro_det;
delete from capacidad_centro_cab;
delete from vehiculo_espera;
delete from vehiculo_rechazado;