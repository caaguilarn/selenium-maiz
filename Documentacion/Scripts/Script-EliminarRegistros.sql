/*
 * 11 setencias sql que permiten eliminar registro ingresados 
 * al ejecutar casos de pruebas automatizados.
 * tabla motivo_cambio 1 registros.
 * tabla observaciones 3 registros.
 * tabla propuesta 2 registros, propuesta detalle 14 registros.
 * tabla programa 1 registro.
 * tabla condicion precio y flete 1 registro, condicion precio y flete_detalle 7 registros. 
 * tabla centro operacion 1 registro, centro operacion_detalle 7 registros.
 * tabla vehiculo en espera 1 registro.
 * tabla vehiculo rechazado 1 registro.
 */

delete from propuesta_motivo_cambio where id_propuesta in (select id_propuesta from propuesta order by 1 desc limit 2);
delete from observaciones  where id_propuesta in(select id_propuesta from propuesta order by 1 desc limit 3);
delete from propuesta_det  where id_propuesta in(select id_propuesta from propuesta order by 1 desc limit 2);
delete from propuesta where id_propuesta in(select id_propuesta from propuesta order by 1 desc limit 2);
delete from programa where id_programa in(select id_programa from programa order by 1 desc limit 1);
delete from  condicion_precio_flete_det where id_condicion_precio_flete_cab in (select id_condicion_precio_flete_cab from condicion_precio_flete_cab order by 1 desc limit 1);
delete from condicion_precio_flete_cab  where id_condicion_precio_flete_cab in (select id_condicion_precio_flete_cab from condicion_precio_flete_cab order by 1 desc limit 1);
delete from capacidad_centro_det where id_capacidad_centro_cab in(select id_capacidad_centro_cab from capacidad_centro_cab order by 1 desc limit 1);
delete from capacidad_centro_cab where id_capacidad_centro_cab in (select id_capacidad_centro_cab from capacidad_centro_cab order by 1 desc limit 1);
delete from vehiculo_espera where id_vehiculo_espera in(select id_vehiculo_espera from vehiculo_espera order by 1 desc limit 1);
delete from vehiculo_rechazado where id_vehiculo_rechazado in (select id_vehiculo_rechazado from vehiculo_rechazado order by 1 desc limit 1);