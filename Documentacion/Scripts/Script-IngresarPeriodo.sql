delete from periodo;

INSERT INTO public.periodo (semana,anio,rango_semana,fecha_ini_semana,fecha_fin_semana,fecha_registro,usuario_registro,fecha_actualizacion,usuario_actualizacion) VALUES 
(1,2020,'30 Diciembre - 05 Enero','2019-12-30','2020-01-05','2020-06-17 00:00:00.000','admin',NULL,NULL)
,(2,2020,'06 Enero - 12 Enero','2020-01-06','2020-01-12','2020-06-17 00:00:00.000','admin',NULL,NULL)
,(3,2020,'13 Enero - 19 Enero','2020-01-13','2020-01-19','2020-06-17 00:00:00.000','admin',NULL,NULL)
,(4,2020,'20 Enero - 26 Enero','2020-01-20','2020-01-26','2020-06-17 00:00:00.000','admin',NULL,NULL)
,(5,2020,'27 Enero - 02 Febrero','2020-01-27','2020-02-02','2020-06-17 00:00:00.000','admin',NULL,NULL)
,(6,2020,'03 Febrero - 09 Febrero','2020-02-03','2020-02-09','2020-06-17 00:00:00.000','admin',NULL,NULL)
,(7,2020,'10 Febrero - 16 Febrero','2020-02-10','2020-02-16','2020-06-17 00:00:00.000','admin',NULL,NULL)
,(8,2020,'17 Febrero - 23 Febrero','2020-02-17','2020-02-23','2020-06-17 00:00:00.000','admin',NULL,NULL)
,(9,2020,'24 Febrero - 01 Marzo','2020-02-24','2020-03-01','2020-06-17 00:00:00.000','admin',NULL,NULL)
,(10,2020,'02 Marzo - 08 Marzo','2020-03-02','2020-03-08','2020-06-17 00:00:00.000','admin',NULL,NULL)
;
INSERT INTO public.periodo (semana,anio,rango_semana,fecha_ini_semana,fecha_fin_semana,fecha_registro,usuario_registro,fecha_actualizacion,usuario_actualizacion) VALUES 
(11,2020,'09 Marzo - 15 Marzo','2020-03-09','2020-03-15','2020-06-17 00:00:00.000','admin',NULL,NULL)
,(12,2020,'16 Marzo - 22 Marzo','2020-03-16','2020-03-22','2020-06-17 00:00:00.000','admin',NULL,NULL)
,(13,2020,'23 Marzo - 29 Marzo','2020-03-23','2020-03-29','2020-06-17 00:00:00.000','admin',NULL,NULL)
,(14,2020,'30 Marzo - 05 Abril','2020-03-30','2020-04-05','2020-06-17 00:00:00.000','admin',NULL,NULL)
,(15,2020,'06 Abril - 12 Abril','2020-04-06','2020-04-12','2020-06-17 00:00:00.000','admin',NULL,NULL)
,(16,2020,'13 Abril - 19 Abril','2020-04-13','2020-04-19','2020-06-17 00:00:00.000','admin',NULL,NULL)
,(17,2020,'20 Abril - 26 Abril','2020-04-20','2020-04-26','2020-06-17 00:00:00.000','admin',NULL,NULL)
,(18,2020,'27 Abril - 03 Mayo','2020-04-27','2020-05-03','2020-06-17 00:00:00.000','admin',NULL,NULL)
,(19,2020,'04 Mayo - 10 Mayo','2020-05-04','2020-05-10','2020-06-17 00:00:00.000','admin',NULL,NULL)
,(20,2020,'11 Mayo - 17 Mayo','2020-05-11','2020-05-17','2020-06-17 00:00:00.000','admin',NULL,NULL)
;
INSERT INTO public.periodo (semana,anio,rango_semana,fecha_ini_semana,fecha_fin_semana,fecha_registro,usuario_registro,fecha_actualizacion,usuario_actualizacion) VALUES 
(21,2020,'18 Mayo - 24 Mayo','2020-05-18','2020-05-24','2020-06-17 00:00:00.000','admin',NULL,NULL)
,(22,2020,'25 Mayo - 31 Mayo','2020-05-25','2020-05-31','2020-06-17 00:00:00.000','admin',NULL,NULL)
,(23,2020,'01 Junio - 07 Junio','2020-06-01','2020-06-07','2020-06-17 00:00:00.000','admin',NULL,NULL)
,(24,2020,'08 Junio - 14 Junio','2020-06-08','2020-06-14','2020-06-17 00:00:00.000','admin',NULL,NULL)
,(25,2020,'15 Junio - 21 Junio','2020-06-15','2020-06-21','2020-06-17 00:00:00.000','admin',NULL,NULL)
,(26,2020,'22 Junio - 28 Junio','2020-06-22','2020-06-28','2020-06-17 00:00:00.000','admin',NULL,NULL)
,(27,2020,'29 Junio - 05 Julio','2020-06-29','2020-07-05','2020-06-17 00:00:00.000','admin',NULL,NULL)
,(28,2020,'06 Julio - 12 Julio','2020-07-06','2020-07-12','2020-06-17 00:00:00.000','admin',NULL,NULL)
,(29,2020,'13 Julio - 19 Julio','2020-07-13','2020-07-19','2020-06-17 00:00:00.000','admin',NULL,NULL)
,(30,2020,'20 Julio - 26 Julio','2020-07-20','2020-07-26','2020-06-17 00:00:00.000','admin',NULL,NULL)
;
INSERT INTO public.periodo (semana,anio,rango_semana,fecha_ini_semana,fecha_fin_semana,fecha_registro,usuario_registro,fecha_actualizacion,usuario_actualizacion) VALUES 
(31,2020,'27 Julio - 02 Agosto','2020-07-27','2020-08-02','2020-06-17 00:00:00.000','admin',NULL,NULL)
,(32,2020,'03 Agosto - 09 Agosto','2020-08-03','2020-08-09','2020-06-17 00:00:00.000','admin',NULL,NULL)
,(33,2020,'10 Agosto - 16 Agosto','2020-08-10','2020-08-16','2020-06-17 00:00:00.000','admin',NULL,NULL)
,(34,2020,'17 Agosto - 23 Agosto','2020-08-17','2020-08-23','2020-06-17 00:00:00.000','admin',NULL,NULL)
,(35,2020,'24 Agosto - 30 Agosto','2020-08-24','2020-08-30','2020-06-17 00:00:00.000','admin',NULL,NULL)
,(36,2020,'31 Agosto - 06 Septiembre','2020-08-31','2020-09-06','2020-06-17 00:00:00.000','admin',NULL,NULL)
,(37,2020,'07 Septiembre - 13 Septiembre','2020-09-07','2020-09-13','2020-06-17 00:00:00.000','admin',NULL,NULL)
,(38,2020,'14 Septiembre - 20 Septiembre','2020-09-14','2020-09-20','2020-06-17 00:00:00.000','admin',NULL,NULL)
,(39,2020,'21 Septiembre - 27 Septiembre','2020-09-21','2020-09-27','2020-06-17 00:00:00.000','admin',NULL,NULL)
,(40,2020,'28 Septiembre - 04 Octubre','2020-09-28','2020-10-04','2020-06-17 00:00:00.000','admin',NULL,NULL)
;
INSERT INTO public.periodo (semana,anio,rango_semana,fecha_ini_semana,fecha_fin_semana,fecha_registro,usuario_registro,fecha_actualizacion,usuario_actualizacion) VALUES 
(41,2020,'05 Octubre - 11 Octubre','2020-10-05','2020-10-11','2020-06-17 00:00:00.000','admin',NULL,NULL)
,(42,2020,'12 Octubre - 18 Octubre','2020-10-12','2020-10-18','2020-06-17 00:00:00.000','admin',NULL,NULL)
,(43,2020,'19 Octubre - 25 Octubre','2020-10-19','2020-10-25','2020-06-17 00:00:00.000','admin',NULL,NULL)
,(44,2020,'26 Octubre - 01 Noviembre','2020-10-26','2020-11-01','2020-06-17 00:00:00.000','admin',NULL,NULL)
,(45,2020,'02 Noviembre - 08 Noviembre','2020-11-02','2020-11-08','2020-06-17 00:00:00.000','admin',NULL,NULL)
,(46,2020,'09 Noviembre - 15 Noviembre','2020-11-09','2020-11-15','2020-06-17 00:00:00.000','admin',NULL,NULL)
,(47,2020,'16 Noviembre - 22 Noviembre','2020-11-16','2020-11-22','2020-06-17 00:00:00.000','admin',NULL,NULL)
,(48,2020,'23 Noviembre - 29 Noviembre','2020-11-23','2020-11-29','2020-06-17 00:00:00.000','admin',NULL,NULL)
,(49,2020,'30 Noviembre - 06 Diciembre','2020-11-30','2020-12-06','2020-06-17 00:00:00.000','admin',NULL,NULL)
,(50,2020,'07 Diciembre - 13 Diciembre','2020-12-07','2020-12-13','2020-06-17 00:00:00.000','admin',NULL,NULL)
;
INSERT INTO public.periodo (semana,anio,rango_semana,fecha_ini_semana,fecha_fin_semana,fecha_registro,usuario_registro,fecha_actualizacion,usuario_actualizacion) VALUES 
(51,2020,'14 Diciembre - 20 Diciembre','2020-12-14','2020-12-20','2020-06-17 00:00:00.000','admin',NULL,NULL)
,(52,2020,'21 Diciembre - 27 Diciembre','2020-12-21','2020-12-27','2020-06-17 00:00:00.000','admin',NULL,NULL)
,(53,2020,'28 Diciembre - 03 Enero','2020-12-28','2021-01-03','2020-06-17 00:00:00.000','admin',NULL,NULL)
;